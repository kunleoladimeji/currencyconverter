<?php
$currencyList;
if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

 	$action = $_GET["action"];
	switch ($action) {
	 	case 'loadDefault':
	 			$returnedValue = loadRates();
	 			echo $returnedValue;
	 		break;
	 	case 'doConversion':
	 			$conversion = doConversion($_GET["currency"]);
	 			echo $conversion;
	 		break;
	 	default:
	 			echo "What are you really doing here? Sod off!";
	 		break;
	 }
}
 function loadRates() {
 	try {

			$converterarray = array();
			$ECB = simplexml_load_file("http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml");
			foreach ($ECB->Cube->Cube->Cube as $rate) {
				$therate = 
				$converterarray["ECB"][(string)$rate["currency"]] = array("to" => (string)$rate["rate"], "inverse" => 1/(float)$rate["rate"]) ;
			} 
			return json_encode($converterarray);
	} catch (Exception $e) {
		return $e->errorMessage();
	}
}

function doConversion($currency)
{
	try {
		$x = file_get_contents('http://rate-exchange-1.appspot.com/currency?from=EUR&to='.$currency);
		$y = json_decode($x);
		foreach ($y as $key => $value) {
			if (is_float($value)) {
				return $value;
			}
		}
	} catch (Exception $e) {
		return $e->errorMessage();
	}
					

}
?>