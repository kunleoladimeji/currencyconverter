# Currency Converter
[Demo](https://goo.gl/WLNMDb)

*Structure*
- **css** (for css files) 
- **img** (for image files)
- **inc** (for PHP include files)
- **js** (for JavaScript files)

----------
# Next Updates
- Use composer for dependencies rather than storing them in files
- Some JavaScript tweaks