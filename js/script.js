

jQuery(document).ready(function($) {
	window.optionValues = [];
	window.selectCurrencyExchangeRate = "";
	$('#selectCurrency option').each(function() {
    optionValues.push($(this).val());
	});
	$.get('inc/converter.php', {"action": "loadDefault"}, function(data) {
		data = JSON.parse(data);
		var l = Object.keys(data.ECB).length;
		for (var i = Object.keys(data.ECB).length - 1; i >= 0; i--) {
			$("div.ticker").append('<div class="ticker__item">EUR/' + Object.keys(data.ECB)[i] + ' = ' + data.ECB[Object.keys(data.ECB)[i]].to + '</div>');
		}
		console.log(data);
	});

	$("#euroFrom").on('paste keyup', function(event) {
		event.preventDefault();
		var inputText = $(this).val();
		var result = inputText * selectCurrencyExchangeRate;
		$("#currencyWanted").val(result.toFixed(2));
	});
	$("#currencyWanted").on('paste keyup', function(event) {
		event.preventDefault();
		var inputText = $(this).val();
		var result = inputText / selectCurrencyExchangeRate;
		$("#euroFrom").val(result.toFixed(2));
	});

	$("#selectCurrency").on('change', function(event) {
		event.preventDefault();
			var currency = $(this).val();
			var currencyFullText = $(this).find(':selected').text();
			console.log("Im here");
			$.get('inc/converter.php', {action: "doConversion", "currency" : currency}, function(data) {
				//Do HTML Manipulation here
				selectCurrencyExchangeRate = data;
				//Header Here
				$("#convertToTitle>h4").text(currencyFullText + ' (' + currency + ')');
				//Table From Here
				$("#convertFrom>tbody>tr").find('[data-title="five"]').text(currency + ' ' + (data * 5).toFixed(2));
				$("#convertFrom>tbody>tr").find('[data-title="ten"]').text(currency + ' ' +(data * 10).toFixed(2));
				$("#convertFrom>tbody>tr").find('[data-title="fifty"]').text(currency + ' ' + (data * 50).toFixed(2));
				$("#convertFrom>tbody>tr").find('[data-title="hundred"]').text(currency + ' ' + (data * 100).toFixed(2));
				$("#convertFrom>tbody>tr").find('[data-title="twofifty"]').text(currency + ' ' + (data * 250).toFixed(2));
				$("#convertFrom>tbody>tr").find('[data-title="fivehundred"]').text(currency + ' ' + (data * 500).toFixed(2));
				$("#convertFrom>tbody>tr").find('[data-title="thousand"]').text(currency + ' ' + (data * 1000).toFixed(2));

				$("#convertTo>thead.cf>tr").children('.numeric').each(function(index, el) {
					switch(index) {
						case 0:
						     $(el).text(currency + ' 5');
						     break;
						case 1:
							$(el).text(currency + ' 10');
							break;
						case 2:
							$(el).text(currency + ' 50');
							break;
						case 3:
							$(el).text(currency + ' 100');
							break;
						case 4:
							$(el).text(currency + ' 250');
							break;
						case 5:
							$(el).text(currency + ' 500');
							break;
						case 6:
							$(el).text(currency + ' 1000');
							break;
					}
				});

				//Table To Here
				$("#convertTo>tbody>tr").find('[data-title="five"]').html('\&euro; ' + (5 / data).toFixed(2));
				$("#convertTo>tbody>tr").find('[data-title="ten"]').html('\&euro; ' +(10 / data).toFixed(2));
				$("#convertTo>tbody>tr").find('[data-title="fifty"]').html('&euro; ' + (50 / data).toFixed(2));
				$("#convertTo>tbody>tr").find('[data-title="hundred"]').html('&euro; ' + (100 / data).toFixed(2));
				$("#convertTo>tbody>tr").find('[data-title="twofifty"]').html( '&euro; ' + (250 / data).toFixed(2));
				$("#convertTo>tbody>tr").find('[data-title="fivehundred"]').html('&euro; ' + (500 / data).toFixed(2));
				$("#convertTo>tbody>tr").find('[data-title="thousand"]').html('&euro; ' + (1000 / data).toFixed(2));
			});
	});
	$("#selectCurrency").val('MUR').change();
});